# Seguridad Ofensiva con hardware libre

Una de las comunidades más fuertes de software libre es aquella que diseña, colabora y educa en ciberseguridad. Los profesionales de seguridad ofensiva (Redteam) se valen de un gran arsenal de herramientas, las cuales en su mayoría incluyen software y hardware libre. Desde el sistema operativo hasta los gadgets más específicos, la comunidad libre es el núcleo del desarrollo de un hacker ofensivo.

## Formato de la propuesta

Indicar uno de estos:

* [x] Charla (25 minutos)
* [ ] Charla relámpago (10 minutos)

## Descripción

La charla pretende ser una introducción al papel del software y hardware libre en ciberseguridad ofensiva y cuáles son algunas de las herramienta s fundamentales libres de un _redteam_.

## Público objetivo

Va dirigida a técnicos y usuarios de Linux con curiosidad por saber cómo funciona la especialidad en seguridad.

## Ponente(s)

Paula de la Hoz, senior Redteam en Telefónica Ingeniería de Seguridad.

### Contacto(s)

* Paula: @terceranexus6 telegram/gitlab

## Comentarios

Necesito proyector y enchufe.

## Condiciones

* [x] Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [x] Al menos una persona entre los que la proponen estará presente el día programado para la charla.
